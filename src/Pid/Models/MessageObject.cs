﻿namespace Coscine.Api.Pid.Models
{
    /// <summary>
    /// Message Object.
    /// </summary>
    public class MessageObject
    {
        /// <summary>
        /// Name of the requester.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Email address of the requester.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Message Text.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Send copy to requester.
        /// </summary>
        public bool SendCopy { get; set; }

        /// <summary>
        /// Pid of the resource.
        /// </summary>
        public string Pid { get; set; }

        /// <summary>
        /// Message Object.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Parameterless constructor for the Message Object.
        /// </summary>
        public MessageObject()
        {

        }

        /// <summary>
        /// Constructor Message Object.
        /// </summary>
        public MessageObject(string name, string email, string message, bool sendCopy, string pid, string guid)
        {
            Name = name;
            Email = email;
            Message = message;
            SendCopy = sendCopy;
            Pid = pid;
            Guid = guid;
        }
    }
}
