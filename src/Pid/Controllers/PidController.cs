using Coscine.Action;
using Coscine.Action.EventArgs;
using Coscine.Api.Pid.Models;
using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Coscine.Api.Pid.Controllers
{
    /// <summary>
    /// This controller represents the actions to contact pid owner.
    /// </summary>
    public class PidController : Controller
    {
        private readonly ResourceModel _resourceModel;
        private readonly ProjectModel _projectModel;
        private readonly IConfiguration _configuration;
        private readonly Emitter _emitter;
        private readonly CoscineLogger _coscineLogger;
        private readonly ProjectRoleModel _projectRoleModel;
        private readonly RoleModel _roleModel;
        private readonly ProjectResourceModel _projectResourceModel;
        private readonly VisibilityModel _visibilityModel;

        /// <summary>
        /// PidController constructor for contacting the pid owner.
        /// </summary>
        public PidController(ILogger<PidController> logger)
        {
            _configuration = Program.Configuration;
            _emitter = new Emitter(_configuration);
            _resourceModel = new ResourceModel();
            _projectModel = new ProjectModel();
            _coscineLogger = new CoscineLogger(logger);
            _projectRoleModel = new ProjectRoleModel();
            _roleModel = new RoleModel();
            _projectResourceModel = new ProjectResourceModel();
            _visibilityModel = new VisibilityModel();
        }

        /// <summary>
        /// Returns a 200, if a PID is valid.
        /// </summary>
        /// <param name="pid">PID</param>
        /// <returns>200 if successful, 400 if pid is badly formatted, 403 if project or resource is deleted, 404 if not exists.</returns>
        [HttpGet("[controller]/valid/{pid}")]
        public IActionResult IsValid(string pid)
        {
            var id = Uri.UnescapeDataString(pid);
            if (id.Contains('/'))
            {
                id = id[(id.IndexOf("/") + 1)..];
            }
            else
            {
                return BadRequest($"PID {pid} is badly formatted.");
            }
            if (id.Contains('@'))
            {
                id = id[..id.IndexOf("@")];
            }
            if (id.Contains('/'))
            {
                id = id[..id.IndexOf("/")];
            }

            Resource resource = null;
            try
            {
                resource = _resourceModel.GetByIdIncludingDeleted(new Guid(id));
            }
            catch
            {

            }
            Project project = null;
            try
            {
                project = _projectModel.GetByIdIncludingDeleted(new Guid(id));
            }
            catch
            {

            }

            if (resource == null && project == null)
            {
                return NotFound($"No project/resource with PID {pid} exists.");
            }

            if ((resource != null && resource.Deleted) || (project != null && project.Deleted))
            {
                return Forbid();
            }

            return Ok();
        }

        /// <summary>
        /// Sends a request to the pid owner.
        /// </summary>
        /// <param name="messageObject">Entry with the information for the email to the pid owner.</param>
        /// <returns>JSON object.</returns>
        [HttpPost("[controller]/sendMailToOwner")]
        public IActionResult SendMailToOwner([FromBody] MessageObject messageObject)
        {
            Resource resource = null;
            try
            {
                resource = _resourceModel.GetByIdIncludingDeleted(new Guid(messageObject.Guid));
            }
            catch
            {

            }
            Project project = null;
            try
            {
                project = _projectModel.GetByIdIncludingDeleted(new Guid(messageObject.Guid));
            }
            catch
            {

            }

            if (resource == null && project == null)
            {
                // Log the error.
                // ProjectId and ResourceId will be null and indicate an error in the logs.
                LogAnalyticsPidEnquiry(null, null, null, null, null, null, null, null);
                return NotFound($"No project/resource with PID {messageObject.Pid} exists.");
            }

            if ((resource != null && resource.Deleted) || (project != null && project.Deleted))
            {
                return Forbid();
            }

            var user = new User()
            {
                DisplayName = messageObject.Name,
                EmailAddress = messageObject.Email
            };
            var placeholder = new JObject()
            {
                ["requesterName"] = messageObject.Name,
                ["requesterMail"] = messageObject.Email,
                ["requestMessage"] = messageObject.Message,
                ["pid"] = messageObject.Pid
            };

            PIDEventArgs pidEventArgs = new PIDEventArgs(_configuration)
            {
                Requester = user,
                SentCopy = messageObject.SendCopy,
                Placeholder = placeholder
            };

            ResourceObject resourceObject = null;
            ProjectObject projectObject = null;
            IEnumerable<ProjectRole> projectOwners = null;

            if (resource != null)
            {
                placeholder["resourceName"] = resource.DisplayName;
                pidEventArgs.Resource = resource;
                resourceObject = _resourceModel.CreateReturnObjectFromDatabaseObject(resource);

                var projectId = _projectResourceModel.GetProjectForResource(resource.Id);

                if (projectId.HasValue)
                {
                    project = _projectModel.GetById(projectId.Value);
                    projectObject = _projectModel.CreateReturnObjectFromDatabaseObject(project);

                    var ownerId = _roleModel.GetAllWhere((x) => (x.DisplayName == "owner")).First().Id;
                    projectOwners = _projectRoleModel.GetAllWhere((x) => x.RoleId == ownerId && x.ProjectId == projectId.Value);
                }
            }
            else if (project != null)
            {
                placeholder["projectName"] = project.DisplayName;
                pidEventArgs.Project = project;
                projectObject = _projectModel.CreateReturnObjectFromDatabaseObject(project);

                var ownerId = _roleModel.GetAllWhere((x) => (x.DisplayName == "owner")).First().Id;
                projectOwners = _projectRoleModel.GetAllWhere((x) => x.RoleId == ownerId && x.ProjectId == project.Id);
            }

            if (CoscineLoggerConfiguration.IsLogLevelActivated(LogType.Analytics))
            {
                LogAnalyticsPidEnquiry(project, resource, resourceObject?.ApplicationProfile, resourceObject?.License?.DisplayName, projectObject?.Disciplines, projectObject?.Organizations, projectOwners, messageObject.Email);
            }

            _emitter.EmitPIDOwnerContact(pidEventArgs);
            return Json(new JObject { ["status"] = "ok" });
        }

        private void LogAnalyticsPidEnquiry(Project project, Resource resource, string applicationsProfile, string license,
        IEnumerable<DisciplineObject> disciplines, IEnumerable<OrganizationObject> organizations, IEnumerable<ProjectRole> owners, string email)
        {
            string visibility = null;
            if (project != null && project.VisibilityId.HasValue)
            {
                visibility = _visibilityModel.GetById(project.VisibilityId.Value)?.DisplayName;
            }
            _coscineLogger.AnalyticsLog(
                new AnalyticsLogObject
                {
                    Type = "Action",
                    Operation = "PID Enquiry",
                    ProjectId = project?.Id.ToString(),
                    ResourceId = resource?.Id.ToString(),
                    ApplicationsProfile = applicationsProfile,
                    License = license,
                    Disciplines = disciplines?.Select(x => x.DisplayNameEn).ToList(),
                    Organizations = organizations?.Select(x => x.DisplayName).ToList(),
                    Visibility = visibility,
                    UserList = owners?.Select(x => x.UserId.ToString()).ToList(),
                    ExternalInfo = email == null ? null : HashMail(email),
                });
        }

        private static string HashMail(string email)
        {
            using var sha256Hash = SHA256.Create();
            var data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(email));
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                stringBuilder.Append(data[i].ToString("x2"));
            }
            return stringBuilder.ToString();
        }
    }
}
